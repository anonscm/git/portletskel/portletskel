package org.evolvis.portletskeleton.demo;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;

import org.evolvis.portletskeleton.VelocityPortlet;

public class VelocityPortletDemo extends VelocityPortlet
{
	@Override
	public void processAction(ActionRequest request, ActionResponse response) throws IOException, PortletException
	{
		String text = request.getParameter("text");
		request.getPortletSession().setAttribute("text", text);

		// parameters that should be carried over to the render phase must be explicitly transferred
		response.setRenderParameters(request.getParameterMap());
		
		response.setPortletMode(PortletMode.VIEW);		
	}
}
