package org.evolvis.portletskeleton.demo.ipc;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;

import org.evolvis.portletskeleton.VelocityPortlet;

public class ShoppingCartPortlet extends VelocityPortlet implements Constants {
	

	@Override
	public void processAction(ActionRequest request, ActionResponse response) throws IOException, PortletException {
		
		//nothing to do here
		
		// parameters that should be carried over to the render phase must be explicitly transferred
		response.setRenderParameters(request.getParameterMap());
		
		response.setPortletMode(PortletMode.VIEW);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processEvent(EventRequest request, EventResponse response)
    		throws PortletException, IOException {
		// get event
    	Event event = request.getEvent();
    	// react to event(s)
    	if (EVENT_ADD_SHOP_ITEM.equals(event.getQName())) {
    		// get event value
    		ShopItem item = (ShopItem) event.getValue();
    		// application specific action
    		List<ShopItem> cart = (List<ShopItem>) request.getPortletSession().getAttribute("shoppingCart");
    		if (cart == null) {
    			cart = new LinkedList<ShopItem>();
    			request.getPortletSession().setAttribute("shoppingCart", cart);
    		}
    		cart.add(item);
    	}
	}

//	@ProcessEvent(qname=EVENT_ADD_SHOP_ITEM)
//	public void processAddShopItem(EventRequest request, EventResponse response)
//	}
}
