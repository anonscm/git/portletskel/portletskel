package org.evolvis.portletskeleton.demo.ipc;

import java.io.Serializable;

public class ShopItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private Integer price;
	
	public ShopItem(String itemStr) {
		int idx = itemStr.indexOf(',');
		this.name = itemStr.substring(0, idx);
		this.price = Integer.parseInt(itemStr.substring(idx+1).trim());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return name+", "+price;
	}
}
