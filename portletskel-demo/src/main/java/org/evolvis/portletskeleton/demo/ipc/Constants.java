package org.evolvis.portletskeleton.demo.ipc;

import javax.xml.namespace.QName;

public interface Constants {
	
	public static final String NAMESPACE_EVENTS = "http://www.tarent.de/events";

	public static final QName EVENT_ADD_SHOP_ITEM =
		new QName(NAMESPACE_EVENTS, "AddShopItem");
}
