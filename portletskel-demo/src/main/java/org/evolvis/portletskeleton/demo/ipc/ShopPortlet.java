package org.evolvis.portletskeleton.demo.ipc;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;

import org.evolvis.portletskeleton.VelocityPortlet;

public class ShopPortlet extends VelocityPortlet implements Constants {

	@Override
	public void processAction(ActionRequest request, ActionResponse response) throws IOException, PortletException {
		String item = request.getParameter("item");
		if (item == null) return;  // nothing to do

		ShopItem shopItem = new ShopItem(item);
		if (shopItem.getName().equals("The Lord of the Rings"))
			; // out of stock, you would probably display some error message
		else
			response.setEvent(EVENT_ADD_SHOP_ITEM, shopItem);  // send event

		// parameters that should be carried over to the render phase must be explicitly transferred
		response.setRenderParameters(request.getParameterMap());

		response.setPortletMode(PortletMode.VIEW);
	}
}
