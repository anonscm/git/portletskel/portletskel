
/*
	Predefined variables are:
	
			bsf		Contains reference to BSFManager instance. Use this to access 
					declared objects by calling bsf.lookupBean("key").
					
	Declared objects are:
	
    		ActionRequest actionRequest		Original portlet action request.
    		ActionResponse actionResponse	Original portlet action response.

    		String basePath					Base path of the deployed portlet.
    		String scriptPath				Base path of the portlet's scripts.
*/

import javax.portlet.PortletMode;

actionRequest = bsf.lookupBean("actionRequest");
actionResponse = bsf.lookupBean("actionResponse");

basePath = bsf.lookupBean("basePath");
scriptPath = bsf.lookupBean("scriptPath");

// parameters that should be carried over to the render phase must be explicitly transferred
actionResponse.setRenderParameters(actionRequest.getParameterMap());

actionRequest.getPortletSession().setAttribute("text", actionRequest.getParameter("text"));

actionResponse.setPortletMode(PortletMode.VIEW);
