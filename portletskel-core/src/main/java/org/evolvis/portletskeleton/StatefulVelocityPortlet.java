/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.evolvis.portletskeleton.VelocityPortlet;

/**
 * This class is an extended VelocityPortlet with util methodes and support
 * for view states.
 * 
 * @see VelocityPortlet
 * 
 * @author Tino Rink (t.rink@tarent.de)
 * @author Jan-Hendrik Peters (j.peters@tarent.de)
 */
public abstract class StatefulVelocityPortlet extends VelocityPortlet {

    /**
     * copy the values of the given parameters from the request to the attribute set of request
     */
    protected void copyParametersToAttributes(final ActionRequest request, final String... parameters) {
        for (final String parameter : parameters) {
            request.setAttribute(parameter, request.getParameter(parameter));
        }
    }

    /**
     * set portlet state
     */
	protected <T extends Serializable> T getState(final PortletRequest request) {
        final PortletSession portletSession = request.getPortletSession();
        return (T) portletSession.getAttribute(getClass().getName() + ".state");
    }

    /**
     * get portlet state
     */
    protected <T extends Serializable> void setState(final PortletRequest request, final T state) {
        final PortletSession portletSession = request.getPortletSession();
        portletSession.setAttribute(getClass().getName() + ".state", state);
    }
    
    /**
     * check if warnings or errors exits
     */
    protected boolean hasWarningsOrErrors(final ActionRequest request) {
        final List warningMsgList = (List) request.getAttribute("warningMsg");
        final List errorMsgList = (List) request.getAttribute("errorMsg");
        return ((null != warningMsgList) && (warningMsgList.size() > 0)) ||
               ((null != errorMsgList) && (errorMsgList.size() > 0));
    }

    /**
     * add a successMsg to the request attributes
     */
    protected void addSuccessMsg(final ActionRequest request, final String successMsg) {
        addMsg(request, "successMsg", successMsg);
    }

    /**
     * add a collection of successMsg to the request attributes
     */
    protected void addSuccessMsg(final ActionRequest request, final Collection<String> successMsgs) {
        addMsg(request, "successMsg", successMsgs);
    }

    /**
     * add a warningMsg to the request attributes
     */
    protected void addWarningMsg(final ActionRequest request, final String warningMsg) {
        addMsg(request, "warningMsg", warningMsg);
    }

    /**
     * add a collection of warningMsg to the request attributes
     */
    protected void addWarningMsg(final ActionRequest request, final Collection<String> warningMsgs) {
        addMsg(request, "warningMsg", warningMsgs);
    }

    /**
     * add a errorMsg to the request attributes
     */
    protected void addErrorMsg(final ActionRequest request, final String errorMsg) {
        addMsg(request, "errorMsg", errorMsg);
    }

    /**
     * add a collection of errorMsg to the request attributes
     */
    protected void addErrorMsg(final ActionRequest request, final Collection<String> errorMsgs) {
        addMsg(request, "errorMsg", errorMsgs);
    }

    private void addMsg(final ActionRequest request, final String msgScope, final String msg) {
        if ((null != request) && (null != msgScope) && (msg != null)) {
            List<String> errors = (List<String>) request.getAttribute(msgScope);
            if (null == errors) {
                errors = new ArrayList<String>();
            }
            errors.add(msg);
            request.setAttribute(msgScope, errors);
        }
    }

    private void addMsg(final ActionRequest request, final String msgScope, final Collection<String> msgs) {
        if ((null != request) && (null != msgScope) && (msgs != null) && !msgs.isEmpty()) {
            List<String> errors = (List<String>) request.getAttribute(msgScope);
            if (null == errors) {
                errors = new ArrayList<String>();
            }
            errors.addAll(msgs);
            request.setAttribute(msgScope, errors);
        }
    }
}
