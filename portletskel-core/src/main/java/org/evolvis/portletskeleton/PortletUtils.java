/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton;

import javax.portlet.ActionRequest;
import javax.portlet.Portlet;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceURL;

public class PortletUtils
{
	private Portlet portlet = null;
	private RenderRequest request = null;
	private RenderResponse response = null;
	
	public PortletUtils(Portlet portlet, RenderRequest request, RenderResponse response)
	{
		this.portlet = portlet;
		this.request = request;
		this.response = response;
	}

	public Portlet getPortlet()
	{
		return portlet;
	}

	public void setPortlet(Portlet portlet)
	{
		this.portlet = portlet;
	}

	public RenderRequest getRequest()
	{
		return request;
	}

	public void setRequest(RenderRequest request)
	{
		this.request = request;
	}

	public RenderResponse getResponse()
	{
		return response;
	}

	public void setResponse(RenderResponse response)
	{
		this.response = response;
	}
	
	public PortletURL createRenderURL()
	{
		return this.response.createRenderURL();
	}
	
	public PortletURL createActionURL()
	{
		return this.response.createActionURL();
	}
	
	public PortletURL createActionURL(String actionName)
	{
		PortletURL portletURL = this.response.createActionURL();
		portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		return portletURL;
	}
	
	public String createActionFormURL()
	{
		return createActionFormURL(null);
	}
	
	public String createActionFormURL(String actionName)
	{
		PortletURL portletURL = this.response.createActionURL();
		if (actionName != null)
			portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		String url = portletURL.toString();
		if (url.contains("?"))
			return url.substring(0, url.indexOf("?"));
		else
			return url;
	}

	public String createActionFormInputFields()
	{
		return createActionFormInputFields(null);
	}
	
	public String createActionFormInputFields(String actionName)
	{
		PortletURL portletURL = this.response.createActionURL();
		if (actionName != null)
			portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		String url = portletURL.toString();
		if (!url.contains("?"))
			return "";
		
		String key, value;
		StringBuffer buffer = new StringBuffer(url.length() * 30);
		for (String part : url.substring(url.indexOf("?") + 1).split("[&]"))
		{
			key = part;
			value = "";
			if (part.contains("="))
			{
				key = part.substring(0, part.indexOf("="));
				value = part.substring(part.indexOf("=") + 1);
			}
			buffer.append("<input type=\"hidden\" name=\"").append(key);
			buffer.append("\" value=\"").append(value).append("\" />\n");
		}
		return buffer.toString();
	}
	
	public ResourceURL createResourceURL()
	{
		return this.response.createResourceURL();
	}
}
