/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton.utils;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Used for localization ability.
 *
 * @author Daniel Ball <d.ball@tarent.de>
 * @author Tino Rink <t.rink@tarent.de>
 */
public class LocaleUtil {

    private static final boolean DEFAULT_GROUPING = true;

    private static final Log log = LogFactory.getLog(LocaleUtil.class);

    private static final ConcurrentHashMap<String, String> bundleKeyLookup = new ConcurrentHashMap<String, String>();

    private String basename;

    private Locale locale;

    private NumberFormat numberformat;

    private LocaleUtil(final String basename, final Locale locale) {
        setBasename(basename);
        setLocale(locale);
    }
    
    /**
     * Create new instance
     *
     * @param basename Basename for {@link ResourceBundle#getBundle(String, Locale) resource bundle}
     * @param locale The locale to use, null for default locale
     */
    public static LocaleUtil getInstance(final String basename, final Locale locale) {
        if (log.isDebugEnabled()) {
            log.debug("getInstance() entered with basename: " + basename + " locale: " + locale);
        }
        if (null == basename) {
            throw new IllegalArgumentException("basename is null");
        }
        return new LocaleUtil(basename, locale);
    }

    /**
     * Create new instance
     * 
     * @param baseclass Use class name as basename
     * @param locale The locale to use, null for default locale
     */
    public static LocaleUtil getInstance(final Class<?> baseclass, final Locale locale) {
        if (log.isDebugEnabled()) {
            log.debug("getInstance() entered with baseclass: " + baseclass + " locale: " + locale);
        }
        if (null == baseclass) {
            throw new IllegalArgumentException("baseclass is null");
        }
        return new LocaleUtil(baseclass.getName(), locale);
    }

    public String getBasename() {
        return basename;
    }

    public void setBasename(final String basename) {
        if (null == basename) {
            throw new IllegalArgumentException("basename is null");
        }
        this.basename = basename;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale != null ? locale : Locale.getDefault();
        numberformat = NumberFormat.getInstance(locale);
    }

    /**
     * Translates key by using stored locale.<br />
     * If translation not found (using {@link ResourceBundle} lookups), the key is returned.
     *
     * @param key
     * @return Translation, or key if no translation found
     */
    public String get(final String key) {
        if (null != key) {
            final String bundleKey = getBundleKey(key);
            try {
                return ResourceBundle.getBundle(basename, locale).getString(bundleKey);
            }
            catch (final MissingResourceException e) {
                log.warn(e);
            }
        }
        // fall-through, may inspect further resources before
        return key;
    }

    /**
     * Cached BundleKey determination.
     * 
     * @param key a not null String
     * @return BundleKey for Key
     */
    private String getBundleKey(final String key) {
        String bundleKey = bundleKeyLookup.get(key);
        if (null == bundleKey) {
            bundleKey = key.replaceAll("\\s+", "_");
            bundleKeyLookup.put(key, bundleKey);
        }
        return bundleKey;
    }

    /**
     * Format number in locale
     *
     * @param n
     * @param useGrouping True to use number group separators
     * @see NumberFormat#format(long)
     * @see NumberFormat#setGroupingUsed(boolean)
     */
    public String format(final long n, final boolean useGrouping) {
        if (numberformat.isGroupingUsed() != useGrouping) {
            numberformat.setGroupingUsed(useGrouping);
        }
        return numberformat.format(n);
    }

    /**
     * Format number in locale with grouping defaults to {@value #DEFAULT_GROUPING}.
     *
     * @param n
     */
    public String format(final long n) {
        return format(n, DEFAULT_GROUPING);
    }

    /**
     * General format with respect to locale
     *
     * @param key The key to locate in language file
     * @param args Arguments to the retrieved pattern
     * @see #get(String)
     * @see MessageFormat#format(String, Object...)
     */
    public String format(final String key, final Object... args) {
        if (null == args) {
            throw new IllegalArgumentException("args must not null");
        }
        return formatPattern(get(key), args);
    }

    /**
     * Workaround for velocity
     *
     * @see #format(String, Object...)
     */
    public String format(final String key, final List<?> args) {
        if (null == args) {
            throw new IllegalArgumentException("args must not null");
        }
        return format(key, args.toArray());
    }

    /**
     * General format with respect to locale
     *
     * @param pattern The pattern used for {@link MessageFormat#format(Object[], StringBuffer, java.text.FieldPosition)
     *        MessageFormat}
     * @param args The arguments for the pattern
     * @see MessageFormat#format(Object[], StringBuffer, java.text.FieldPosition)
     */
    public String formatPattern(final String pattern, final Object... args) {
        return new MessageFormat(pattern, locale).format(args, new StringBuffer(), null).toString();
    }

    /**
     * Workaround for velocity
     *
     * @see #formatPattern(String, Object...)
     */
    public String formatPattern(final String pattern, final List<?> args) {
        return formatPattern(pattern, args.toArray());
    }

    /**
     * Same as {@link #format(String, Object...) but using first object in args as key}
     *
     * @see #format(String, Object...)
     * @param args Array where first item is the key, remaining items are arguments
     */
    public String formatArgs(final Object... args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Arguments are empty");
        }
        return format(args[0].toString(), copyArrayRange(args, 1, args.length));
    }

    /**
     * @see #formatArgs(Object...)
     * @param args
     */
    public String formatArgs(final List<?> args) {
        return formatArgs(args.toArray());
    }

    /**
     * Simulate Java 6 Arrays.copyOfRange()
     *
     * @param original
     * @param from
     * @param to
     * @return Copy of original in range from .. to exclusive
     */
    private static Object[] copyArrayRange(final Object[] original, final int from, final int to)
            throws IllegalArgumentException {
        if (from < 0) {
            throw new IllegalArgumentException("from must not be negative");
        }
        if (to > original.length) {
            throw new IllegalArgumentException("to is greater than original length " + original.length);
        }
        if (from > to) {
            throw new IllegalArgumentException("from is greater than to");
        }

        final Object[] copy = new Object[to - from];

        for (int i = from; i < to; i++) {
            copy[i - from] = original[i];
        }

        return copy;
    }

}
