/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.apache.bsf.BSFManager;
import org.apache.bsf.util.IOUtils;

public class VelocityScriptPortlet extends VelocityPortlet
{
	// path to scripts
	public static final String SCRIPT_PATH = "WEB-INF/scripts";
	
	// pre-defined variables
	public static final String PARAM_SCRIPT_BASE_PATH = "basePath";
	public static final String PARAM_SCRIPT_PATH = "scriptPath";
	
	// portlet configuration keys
	public static final String INIT_PARAM_SCRIPT_ENTRY = "org.evolvis.portletskeleton.script.entry";
	
	@Override
	public void processAction(ActionRequest request, ActionResponse response) throws IOException, PortletException
	{
		// read script location from configuration
		String scriptFile = getPortletContext().getRealPath(
				SCRIPT_PATH + "/" + getPortletName().toLowerCase() + "/" + this.getInitParameter(INIT_PARAM_SCRIPT_ENTRY));
		if (!(new File(scriptFile).canRead()))
			throw new PortletException("Can't access script file " + scriptFile + " for reading.");
		
		// the BSFManager has to be unique for every request
		BSFManager bsfManager = new BSFManager();

    	try
        {    		
    		// declare request and response
    		bsfManager.declareBean("actionRequest", request, ActionRequest.class);
    		bsfManager.declareBean("actionResponse", response, ActionResponse.class);

    		// declare base and script paths for loading more resources from the script
    		bsfManager.declareBean(PARAM_SCRIPT_BASE_PATH, getPortletContext().getRealPath("/"), String.class);
    		bsfManager.declareBean(PARAM_SCRIPT_PATH, getPortletContext().getRealPath(SCRIPT_PATH), String.class);
    		
    		Logger.getLogger(this.getClass().getCanonicalName()).info("Running script " + scriptFile + ".");
    		
    		// execute script, autodetecting the used language from the file extension
    		bsfManager.exec(BSFManager.getLangFromFilename(scriptFile), scriptFile, 0, 0, 
    				IOUtils.getStringFromReader(new FileReader(scriptFile)));
        }
    	catch(Exception e)
        {
    		// FIXME: an exception results in an unspecified 'java.lang.NoClassDefFoundError: org/apache/log4j/spi/ThrowableInformation' in Liferay. Fix this, then remove the printStackTrace().
    		e.printStackTrace();
    		throw new PortletException(e);
        }
	}
}
