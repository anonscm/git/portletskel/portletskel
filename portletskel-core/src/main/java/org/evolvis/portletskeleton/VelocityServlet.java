/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.Portlet;
import javax.portlet.PortletConfig;
import javax.portlet.PortletMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.tools.view.servlet.VelocityViewServlet;
import org.evolvis.portletskeleton.utils.LocaleUtil;

/**
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 * @author Daniel Ball (d.ball@tarent.de)
 * @author Tino Rink (t.rink@tarent.de)
 */
public class VelocityServlet extends VelocityViewServlet {

    // path to velocity templates
    public static final String TEMPLATE_PATH = "WEB-INF" + VelocityPortlet.VELOCITY_BASE_PATH;

    private static final String INITPARAM_RESOURCE_PATH = VelocityServlet.class.getName() + ".resource.path";

    // Possible portal attribute accessor keys
    public static final String JAVAX_PORTLET_CONFIG = "javax.portlet.config";

    public static final String JAVAX_PORTLET_PORTLET = "javax.portlet.portlet";

    public static final String JAVAX_PORTLET_REQUEST = "javax.portlet.request";

    public static final String JAVAX_PORTLET_RESPONSE = "javax.portlet.response";

    // Velocity context attribute names
    public static final String JAVAX_PORTLET_CONFIG_VARIABLE = "portletConfig";

    public static final String JAVAX_PORTLET_PORTLET_VARIABLE = "portlet";

    public static final String JAVAX_PORTLET_REQUEST_VARIABLE = "portletRequest";

    public static final String JAVAX_PORTLET_RESPONSE_VARIABLE = "portletResponse";

    public static final String PORTLET_UTILS_VARIABLE = "portletUtils";

    public static final String PORTLET_MODE_VARIABLE = "portletMode";

    public static final String PORTLET_LOCALE_VARIABLE = "portletLocale";

    public static final String PORTLET_PREFERENCES_VARIABLE = "portletPreferences";

    public static final String PORTLET_SESSION_VARIABLE = "portletSession";

    public static final String PORTLET_WINDOW_STATE_VARIABLE = "portletWindowState";

    public static final String PORTLET_REQUEST_PARAMETERS = "portletParameters";

    public static final String PORTLET_CONTEXT_PATH = "contextPath";

    public static final String PORTLET_NAMESPACE = "namespace";

    public static final String PORTLET_BASE_PATH = "basePath";

    public static final String VELOCITY_TEMPLATE_PATH = "templatePath";

    public static final String VELOCITY_SERVLET = "velocityServlet";

    public static final String LOCALE_UTILS = "tr";

    // Portlet mode strings
    public static final String PORTLET_MODE_VIEW = "VIEW";

    public static final String PORTLET_MODE_EDIT = "EDIT";

    public static final String PORTLET_MODE_HELP = "HELP";

    // Portlet window states
    public static final String WINDOW_STATE_NORMAL = "NORMAL";

    public static final String WINDOW_STATE_MAXIMIZED = "MAXIMIZED";

    public static final String WINDOW_STATE_MINIMIZED = "MINIMIZED";

    private static final long serialVersionUID = 1111858315750266981L;

    private static final Map<String, String> localeBasenameLookup = new ConcurrentHashMap<String, String>();

    /**
     * Path to locale resources, defaults to {@value VelocityServlet#RESOURCE_PATH}.
     * Can be changed by servlet init parameter {@value VelocityServlet#INITPARAM_RESOURCE_PATH}.
     */
    private String RESOURCE_PATH = "/";

    @Override
    protected void initVelocity(ServletConfig ctx) throws ServletException {
        try {
            VelocityEngine velocityEngine = new VelocityEngine();
            velocityEngine.setProperty("file.resource.loader.path", getServletContext().getRealPath(TEMPLATE_PATH));
            velocityEngine.init();
            setVelocityEngine(velocityEngine);

            if (ctx.getInitParameter(INITPARAM_RESOURCE_PATH) != null) {
                RESOURCE_PATH = ctx.getInitParameter(INITPARAM_RESOURCE_PATH);
                if (!RESOURCE_PATH.endsWith("/")) RESOURCE_PATH += "/";
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void fillContext(Context context, HttpServletRequest request) {
        // add portlet specific pre-defined variables to the velocity context

        RenderRequest portletRequest = (RenderRequest) request.getAttribute(JAVAX_PORTLET_REQUEST);
        RenderResponse portletResponse = (RenderResponse) request.getAttribute(JAVAX_PORTLET_RESPONSE);
        PortletConfig portletConfig = (PortletConfig) request.getAttribute(JAVAX_PORTLET_CONFIG);
        Portlet portlet = (Portlet) request.getAttribute(JAVAX_PORTLET_PORTLET);

        // add reference to actual portlet instance
        context.put(JAVAX_PORTLET_PORTLET_VARIABLE, portlet);

        // add original portlet request
        context.put(JAVAX_PORTLET_REQUEST_VARIABLE, portletRequest);

        // add original portlet response
        context.put(JAVAX_PORTLET_RESPONSE_VARIABLE, portletResponse);

        // add portlet config
        context.put(JAVAX_PORTLET_CONFIG_VARIABLE, portletConfig);

        // add util class instance to context, not using the velocity tools mechanism for simplicity
        context.put(PORTLET_UTILS_VARIABLE, new PortletUtils(portlet, portletRequest, portletResponse));

        // add mode - unknown modes will be identified by portletMode.toString()
        if (portletRequest.getPortletMode().equals(PortletMode.VIEW))
            context.put(PORTLET_MODE_VARIABLE, PORTLET_MODE_VIEW);
        else if (portletRequest.getPortletMode().equals(PortletMode.EDIT))
            context.put(PORTLET_MODE_VARIABLE, PORTLET_MODE_EDIT);
        else if (portletRequest.getPortletMode().equals(PortletMode.HELP))
            context.put(PORTLET_MODE_VARIABLE, PORTLET_MODE_HELP);
        else
            context.put(PORTLET_MODE_VARIABLE, portletRequest.getPortletMode().toString());

        // add locale in ISO 3166 letter code
        context.put(PORTLET_LOCALE_VARIABLE, portletRequest.getLocale().getCountry());

        // add preferences
        context.put(PORTLET_PREFERENCES_VARIABLE, portletRequest.getPreferences());

        // add session, creating a new one if none is available
        context.put(PORTLET_SESSION_VARIABLE, portletRequest.getPortletSession());

        // add window state
        if (portletRequest.getWindowState().equals(WindowState.NORMAL))
            context.put(PORTLET_WINDOW_STATE_VARIABLE, WINDOW_STATE_NORMAL);
        else if (portletRequest.getWindowState().equals(WindowState.MAXIMIZED))
            context.put(PORTLET_WINDOW_STATE_VARIABLE, WINDOW_STATE_MAXIMIZED);
        else if (portletRequest.getWindowState().equals(WindowState.MINIMIZED))
            context.put(PORTLET_WINDOW_STATE_VARIABLE, WINDOW_STATE_MINIMIZED);

        // add request parameter map
        context.put(PORTLET_REQUEST_PARAMETERS, portletRequest.getParameterMap());

        // add path information
        context.put(PORTLET_CONTEXT_PATH, portletRequest.getContextPath());
        context.put(PORTLET_NAMESPACE, portletResponse.getNamespace());
        context.put(PORTLET_BASE_PATH, getServletContext().getRealPath("/"));
        context.put(VELOCITY_TEMPLATE_PATH, getServletContext().getRealPath(TEMPLATE_PATH));

        // add locale util
        final LocaleUtil localUtil = LocaleUtil.getInstance(getLocaleBasename(request.getPathInfo()), request.getLocale());
        context.put(LOCALE_UTILS, localUtil);

        // add the servlet itself
        context.put(VELOCITY_SERVLET, this);

        super.fillContext(context, request);
    }

    /**
     * Creates resource basename based on servlets path
     * @param path Servlet path retrieved by {@link HttpServletRequest#getPathInfo()}
     */
    public String getLocaleBasename(final String path) {

        String localeBasename = localeBasenameLookup.get(path);

        if (null == localeBasename) {
            
            String base = extractTemplatePath(path);

            // cut off file extension if available
            final int dotIndex = base.indexOf(".");
            if (dotIndex > 0) {
                base = base.substring(0, dotIndex);
            }

            localeBasename = RESOURCE_PATH + base;
            localeBasenameLookup.put(path, localeBasename);
        }
        return localeBasename;
    }

    /**
     * Extracts the relative path of a template from the servlet request path.
     * The resulting path is relative to VelocityServlet.TEMPLATE_PATH.
     *
     * @param sourcePath Sevlet request path
     * @return relative path to template.
     */
    public static String extractTemplatePath(String sourcePath) {
        // strip leading '/' from path
        if (sourcePath.startsWith("/"))
            return sourcePath.substring(1);
        else
            return sourcePath;
    }

    @Override
    protected Template getTemplate(HttpServletRequest request, HttpServletResponse response)
            throws ResourceNotFoundException, ParseErrorException, Exception {

        final String velocityTemplatePath = extractTemplatePath(request.getPathInfo());

        // let the velocity engine retrieve the template
        return getVelocityEngine().getTemplate(velocityTemplatePath);
    }
}
