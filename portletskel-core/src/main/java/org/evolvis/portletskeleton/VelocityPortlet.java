/*
 * tarent Portlet Skeleton
 * tarent Portlet Specification
 * Copyright (c) 2008-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.evolvis.portletskeleton;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class implements the base portlet skeleton for all velocity-based
 * portlets. Use this class by subclassing it and overload the processAction
 * method, adding your business code. All template handling is done by this
 * base class. The render requests will be delegated to an underlying
 * velocity template. The template path is calculated from a base template path,
 * the request's locale and mode. Custom modes are supported.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class VelocityPortlet extends GenericPortlet
{
	/** Base path for all velocity files */
	public static final String VELOCITY_BASE_PATH = "/velocity";
	public static final String DEFAULT_VELOCITY_EXT = ".vm";

	public static final String INIT_PARAM_THEME_NAME = "org.evolvis.portletskeleton.theme.name";

	public static final String REQUEST_ATTR_TEMPLATE_PATH = "org.evolvis.portletskeleton.velocity.template";

	public final Log log = LogFactory.getLog(getClass());



	private boolean directoryExists(String path)
	{
		File file = new File(path);
		return file.isDirectory() && file.canRead();
	}

	/**
	 * Checks if a path to a template exits by using VelocityServlet to get the real path
	 * and then pass the real path to directoryExits.
	 * @param checkTemplatePath
	 * @return boolean true if path is a directory and readable, false in all other cases
	 */
    private boolean templatePathExits(String checkTemplatePath) {
		// calculate real path, delegate this to VelocityServlet: it knows where to find the templates
    	String realPath = getPortletContext().getRealPath(
				VelocityServlet.TEMPLATE_PATH + "/" + VelocityServlet.extractTemplatePath(checkTemplatePath)
				);

		// check if path exists
		if (directoryExists(realPath))
			return true;
		else
			return false;
	}



	/**
	 * Generates the velocity template path for the given portlet request.
	 * The returned path is build from the velocity template prefix plus the
	 * current locale (as ISO 3166 2-letter country code) plus the given modes
	 * template. If the mode is unknown (custom mode), PortletMode.toString() plus
	 * ".vm" is used.
	 *
	 * @param request RenderRequest of the current request.
	 * @return String describing the template path.
	 */
    protected String calculateTemplatePath(PortletRequest request)
	{
    	// create basic path
    	StringBuffer pathBuffer = new StringBuffer();
    	pathBuffer.append('/');
    	pathBuffer.append(this.getPortletName().toLowerCase());
    	pathBuffer.append('/');

    	// check if themes are enabled and add directory path to pathBuffer
    	String themeName = this.getInitParameter(INIT_PARAM_THEME_NAME);
    	if (themeName!=null && templatePathExits(pathBuffer.toString() + themeName))
    	{
			pathBuffer.append(themeName);
			pathBuffer.append('/');
    	}

		pathBuffer.append(request.getPortletMode().toString()).append(DEFAULT_VELOCITY_EXT);

    	return pathBuffer.toString();
	}


	/**
     * Handles the actual portlet request by delegating the request to the underlying
     * velocity servlet. The template used by the velocity servlet is calculated from
     * the request parameters.
     *
     * @param request Portlet render request.
     * @param response Portlet render response.
     * @throws IOException
     * @throws PortletException
     */
    protected void handleRequest(RenderRequest request, RenderResponse response) throws IOException, PortletException
    {
    	// use the preferred content type
    	response.setContentType(request.getResponseContentType());

    	// delegate the request to the velocity servlet
    	final String path = VELOCITY_BASE_PATH + getTemplatePath(request);
        getPortletContext().getRequestDispatcher(path).include(request, response);
    }

    /**
     * Get template path.
     * Previous calls to {@link #setTemplate(String, PortletRequest)} are regarded.
     * @param request Used to retrieve previously stored template paths
     * @return Stored template path or default template path based on portlet mode
     */
    private String getTemplatePath(final PortletRequest request) {
    	final Object obj;

    	if (request != null && (obj = request.getAttribute(REQUEST_ATTR_TEMPLATE_PATH)) != null && obj instanceof String) {
    		return (String) obj;
    	}

    	return calculateTemplatePath(request);
    }

    @Override
    public void doView(RenderRequest request, RenderResponse response) throws IOException, PortletException
    {
    	handleRequest(request, response);
    }

    @Override
    public void doEdit(RenderRequest request, RenderResponse response) throws IOException, PortletException
    {
    	handleRequest(request, response);
    }

    @Override
    public void doHelp(RenderRequest request, RenderResponse response) throws IOException, PortletException
    {
    	handleRequest(request, response);
    }


    /**
     * Set new template path based on old path.
     * @param path New template path (should be relative without extension)
     * @param request The path is stored here
     * @throws IllegalArgumentException If path is invalid
     */
    public void setTemplate(final String path, final PortletRequest request) throws IllegalArgumentException {
    	if (path != null) {
	    	final URI base = URI.create(getTemplatePath(request));

	    	String newpath = base.resolve(path).normalize().getPath();
	    	final String file = new File(newpath).getName();
	    	if (file.length() == 0) throw new IllegalArgumentException("Invalid path");

	    	if (!file.contains(".")) newpath += DEFAULT_VELOCITY_EXT;

	    	request.setAttribute(REQUEST_ATTR_TEMPLATE_PATH, newpath);
	    	if (log.isDebugEnabled()) log.debug("Set template to "+newpath+" from path "+path);
    	}
    	else {
    		request.removeAttribute(REQUEST_ATTR_TEMPLATE_PATH);
    		log.debug("Restored original template path");
    	}
    }
}
